# terraform-aws-cloudfront-s3-cdn

Terraform module to provision an AWS CloudFront CDN with an S3 or custom origin.
